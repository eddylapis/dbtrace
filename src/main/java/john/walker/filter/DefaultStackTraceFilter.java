package john.walker.filter;



/**
 * 过滤工具自身的调用栈
 *
 * @author 30san
 *
 */
public class DefaultStackTraceFilter implements StackTraceFilter {

	@Override
	public boolean accpet(String className, String methodName) {
		if((className.equals("java.lang.Thread") && methodName.equals("getStackTrace")) ||
				className.startsWith("john.walker")) {
			return false;
		}
		return true;
	}

}
